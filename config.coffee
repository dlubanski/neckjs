exports.config =
  
  files:
    javascripts:
      joinTo:
        'js/app.js': /^app/
        'js/vendor.js': /(^vendor|bower_components)/
        'test/js/test.js': /^test(\/|\\)(?!vendor)/
        'test/js/test-vendor.js': /^test(\/|\\)(?=vendor)/
      order:
        before: [
          "bower_components/jquery/jquery.js"
          "bower_components/underscore/underscore.js"
          "vendor/neck/neck.coffee"
        ]

    stylesheets:
      joinTo:
        'css/app.css': /^app/
        'test/css/test.css': /^test/
      order:
        before: []
        after: []

    templates:
      joinTo: 'js/app.js'

Neck.Binder.Route =

  route: (path, options = {})->
    # Check if parents has no that path
    controller = @root().childWithPath path

    unless controller
      options.path or= path

      try
        controller = new (require("#{Neck.Globals.controllersPath}/#{path}"))(options)
      catch e
        controller = new Neck.Screen(options)
      
      if controller.popup or controller.view is false
        parent = @leaf()
      else
        parent = @

      # Release old path
      parent.child.release() if parent.child
      # Set child of controller
      parent.child = controller
      # Reference to new controller
      controller.parent = parent
      
    # Activate controller
    controller.activate()
      
  _bindRoute: ->
    unless @el[0].bindRoute
      @el[0].bindRoute = true

      @el.on 'click', '[ui-route]', (e)=>
        e.preventDefault()
        e.stopPropagation()

        options = {}

        for key, value of e.currentTarget.dataset
          options[key] = eval Neck.Tools.parseForEval value

        @route e.currentTarget.getAttribute('ui-route'), options

      
  
Neck.Binder.Element =

  _bindElement: ->
    for el in @el.find('[ui-element]')
      el = $(el)
      
      @["$" + el.attr('ui-element')] = el

      # When bind finished remove attr 
      # for not including on next refresh
      el.removeAttr 'ui-element'
Neck.Binder.Helper =
  
  _bindHelper: ->
    for el in @el.find('[ui-helper]')
      el = $(el)
      options = {}

      for key, value of el[0].dataset
        options[key] = eval Neck.Tools.parseForEval value, 'this'
        
      options.el = el
      options.context = @

      new (require("#{Neck.Globals.helpersPath}/" + el.attr('ui-helper')))(options)

      # When bind finished remove attr 
      # for not including on next refresh
      el.removeAttr 'ui-helper'
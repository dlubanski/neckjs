### HELPERS ###

triggerDeepMethod = (string, options)->
  result = @
  list = string.trim().split('.')
  method = list.pop()

  for path in list
    result = result[path]

  result[method](options)

triggerEvent = (attr)->
  return (e)=>
    e.preventDefault()
    e.stopPropagation()
    
    target = e.currentTarget

    options = {}

    for key, value of target.dataset
      options[key] = eval Neck.Tools.parseForEval value

    options.e = e

    triggerDeepMethod.call @, target.getAttribute(attr), options

### LIST OF EVENTS TO TRIGGER ###

EventList = [
  "click"
  "dblclick"

  "mouseenter"
  "mouseleave"
  "mouseout"
  "mouseover"
  "mousedown"
  "mouseup"

  "drag"
  "dragstart"
  "dragenter"
  "dragleave"
  "dragover"
  "dragend"
  "drop"

  "load"

  "focus"
  "focusin"
  "focusout"
  "select"
  "blur"

  "submit"

  "scroll"

  # Touch screen events
  "touchstart"
  "touchend"
  "touchmove"
  "touchenter"
  "touchleave"
  "touchcancel"
]

### MODULE ###

Neck.Binder.Event =

  _bindEvent: ->
    unless @el[0].bindedEvents
      @el[0].bindedEvents = true

      # EVENTS
      for ev in EventList
        @el.on ev, "[ui-event-#{ev}]", triggerEvent.call @, "ui-event-#{ev}"

### HELPERS ###

class ModelHelper extends Spine.Controller

  events: 
    "keyup": "update"
    "change": "update"

  constructor: ->
    super
    # Set default value
    @el.val(@model[@property]) if @model[@property]

  update: ->
    @model[@property] = @el.val()

### MODULE ###

Neck.Binder.Model =

  _bindModel: ->
    for el in @el.find('[ui-model]')
      el = $(el)

      # Find model in controller
      model = @
      list = el.attr('ui-model').trim().split('.')
      property = list.pop()

      for path in list
        model = model[path]

      new ModelHelper el: el, model: model, property: property

      # When bind finished remove attr 
      # for not including on next refresh
      el.removeAttr 'ui-model'
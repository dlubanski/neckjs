Neck.Screen = class Screen extends Spine.Controller

  for binder of Neck.Binder
    @include Neck.Binder[binder]

  className: 'screen'

  path: null
  view: null
  
  # Type of screen (normal/popup)
  popup: false

  transition: 0

  # Screen referances
  parent: null
  child: null

  # DOM flag
  inDOM: false

  # PUBLIC
  constructor: ->
    super

    if @path
      @el.addClass @path.replace /\//gi, ' '
      if @view is null
        @view = @path

    for binder of Neck.Binder
      if _.isFunction @["_bind#{binder}"] 
        @bind 'binder:refresh', @["_bind#{binder}"]

  render: (force = false)->
    if @view or force
      @el.html (require("#{Neck.Globals.viewsPath}/#{@view}"))(@)

      # If there is parent, try to append controller to parent
      @parent?.append @

      @inDOM = true

    @trigger 'binder:refresh'
    @trigger 'view:refresh'
        
    @

  append: (screen)->
    if @$yield
      @$yield.append screen.el
    else
      @parent.append screen

    @

  activate: ->
    # Try to release child
    @child?.release()
    @child = null

    # Deactivate parent
    @parent?.deactivate() unless @popup

    # Change activate property
    @_active = true

    # Set DOM changes
    @el.addClass 'active'
    @el.css 'zIndex', @zIndex = @parent?.zIndex + 1 or 1

    @trigger 'screen:activate'

    # Render controller view
    @render() unless @inDOM

    @

  deactivate: ->
    @_active = false

    # If controller has yield can't be deactivate by
    # children, because it will be inside parent
    unless @$yield
      @el.removeClass 'active'

    @trigger 'screen:deactivate'

    @

  isActive: ->
    @_active is true

  root: ->
    if @parent then @parent.root() else @

  leaf: ->
    if @child then @child.leaf() else @

  childWithPath: (path)->
    if @child
      if @child.path is path
        return @child
      else
        @child.childWithPath path
    else
      return false

  singleton: -> 
    unless @constructor._instance
      @constructor._instance = @
      return false
    else
      return @constructor._instance

  isSingleton: ->
    @constructor._instance isnt undefined

  return:->
    unless @isSingleton()
      @parent?.activate()

  release: ->
    # Firstly release child screens 
    @child?.release()

    unless @isSingleton()
      if @transition
        @deactivate()
        # Kill controller after animation
        setTimeout => 
          super
        , @transition
      else
        # Deactivate not need, super will kill controller
        super
    else
      @el.css 'zIndex', @zIndex = 0
      @deactivate()
        
    @trigger 'screen:release'

    # Clear reference to parent and child screen if exists
    @parent = undefined
    @child = undefined

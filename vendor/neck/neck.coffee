# Create global object
@Neck = Neck = {}

# Globals
Neck.Globals = {}

# Default globals
Neck.Globals = 
  viewsPath: 'views'
  controllersPath: 'controllers'
  helpersPath: 'helpers'

# Common tools
Neck.Tools = {}

Neck.Tools.parseForEval = (el, thisElement = '_this')->
  texts = el.match /\'[^\']+\'/g

  el.replace(/\'[^\']+\'/g, (text, index)-> "###")
    .replace(/([a-zA-Z][^\ ]+)/gi, "#{thisElement}.\$1")
    .replace(/###/g, ()-> texts.shift())

# Create object for binders
Neck.Binder = {}



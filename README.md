# Brunch with Spine Template
Simple template of brunch with spinejs framework specialy created for PhoneGap application boilerplate.

## Getting started
You'll need [brunch](https://github.com/brunch/brunch). Then use in terminal:

	brunch new app_name --skeleton https://github.com/smalluban/brunch-with-spine.git

More info about how brunch works on thier github site.
# Main App controller
class App extends Neck.Screen

  constructor: ->
    super

    # Open root screen when app is ready
    @one 'view:refresh', ->
      @route 'example'

    # Turn App on!
    @activate()

# Start application after document is ready 
# and connect it with body tag
$(document).ready -> new App el: $("body")